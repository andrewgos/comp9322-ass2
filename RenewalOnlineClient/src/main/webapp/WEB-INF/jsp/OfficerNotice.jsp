<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<title>RMS Online Client - Officer Notice Details</title>
</head>
<body>
	<div class="container">
		<c:if test="${not empty alert}">
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<c:out value="${alert}" />
		</div>
		</c:if>
		
		<div class="page-header">
			<h1>RMS Renewal Online System</h1>
		</div>
		
		<div class="row">
			<div class="col-sm-1">
				<form class="" action="http://localhost:8080/RenewalOnlineClient/officer/index">
					<input type="submit" class="btn btn-info" value="Back" />
				</form>
			</div>
		</div>
		
		<br/>
		<br/>
		
		<ul class="nav nav-tabs">
			<li class="active"><a href="#pane1" data-toggle="tab">Notice</a></li>
			<li><a href="#pane2" data-toggle="tab">Registration Details</a></li>
			<li><a href="#pane3" data-toggle="tab">Driver Details</a></li>
		</ul>
		
		<div class="tab-content">
			<div id="pane1" class="tab-pane active" style="height: 200px;">
				<form class="form-horizontal" method="post" action="notice">
					<div class="form-group">
						<label class="col-md-2 control-label">Notice ID</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${noticeId}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Notice Status</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${noticeStatus}" /></label>
						</div>
					</div>
					
					<c:if test="${noticeStatus eq 'REQUESTED'}">
					<div class="form-group">
						<div class="col-md-4 col-md-offset-2">
							<input type="hidden" name="noticeId" value="${noticeId}" />
							<button type="submit" name="btnSubmit" class="btn btn-primary" value="autoCheck">Run Auto Check</button>
						</div>
					</div>
					</c:if>
					
					<c:if test="${noticeStatus eq 'UNDER_REVIEW'}">
					<div class="form-group">
						<label class="col-md-2 control-label">Auto Check Approved</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${checkApproved}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Auto Check Message</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${checkDetails}" /></label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-2">
							<input type="hidden" name="noticeId" value="${noticeId}" />
							<button type="submit" name="btnSubmit" class="btn btn-success" value="accept">Accept</button>
							<button type="submit" name="btnSubmit" class="btn btn-danger" value="reject">Reject</button>
						</div>
					</div>
					</c:if>
					
					<c:if test="${noticeStatus eq 'ACCEPTED'}">
					<div class="form-group">
						<label class="col-md-2 control-label">Fee</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="txtAmount">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4 col-md-offset-2">
							<input type="hidden" name="noticeId" value="${noticeId}" />
							<button type="submit" name="btnSubmit" class="btn btn-primary" value="setFee">Set Amount to Pay</button>
						</div>
					</div>
					</c:if>
					
				</form>
			</div>
			
			<div id="pane2" class="tab-pane" style="height: 200px;">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-md-2 control-label">Registration ID</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${regoId}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Registration Number</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${regoNumber}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Registration Valid Until</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${regoValidity}" /></label>
						</div>
					</div>
				</form>
			</div>
			
			<div id="pane3" class="tab-pane" style="height: 200px;">
				<form class="form-horizontal">
    				<div class="form-group">
						<label class="col-md-2 control-label">First Name</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${driverFirstName}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Last Name</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${driverLastName}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Email</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${driverEmail}" /></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">License Number</label>
						<div class="col-md-4">
							<label class="control-label"><c:out value="${driverLicenseNumber}" /></label>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<hr />
	</div>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>