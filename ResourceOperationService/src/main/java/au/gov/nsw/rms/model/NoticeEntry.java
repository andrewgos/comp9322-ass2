package au.gov.nsw.rms.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Entry")
public class NoticeEntry {
    private String _nid;
    private String _rid;
    private String Status;
    
    
	public String get_nid() {
		return _nid;
	}
	@XmlElement(name="_nid")
	public void set_nid(String _nid) {
		this._nid = _nid;
	}
	public String get_rid() {
		return _rid;
	}
	@XmlElement(name="_rid")
	public void set_rid(String _rid) {
		this._rid = _rid;
	}
	public String getStatus() {
		return Status;
	}
	@XmlElement(name="Status")
	public void setStatus(String status) {
		Status = status;
	}


}
