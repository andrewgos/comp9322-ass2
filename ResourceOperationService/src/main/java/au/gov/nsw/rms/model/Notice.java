package au.gov.nsw.rms.model;

import java.util.List;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Notices")
public class Notice
{
	private List<NoticeEntry> entries;

	public List<NoticeEntry> getEntries() {
		return entries;
	}
	
	public void addEntry(NoticeEntry newNoticeEntry){
		entries.add(newNoticeEntry);
	}

	@XmlElement(name = "Entry")
	public void setEntries(List<NoticeEntry> entries) {
		this.entries = entries;
	}

	public NoticeEntry getNoticeEntry(String nid){
		for (NoticeEntry noticeEntry : entries){
			if(noticeEntry.get_nid().equalsIgnoreCase(nid)){
				return noticeEntry;
			}
		}
		return null;
	}

	
	public enum Status
	{
		CREATED,
		REQUESTED,
		UNDER_REVIEW,
		ACCEPTED,
		REJECTED,
		AWAITING_PAYMENT,
		CANCELLED,
		ARCHIVED,
		COMPLETED
	}
}
