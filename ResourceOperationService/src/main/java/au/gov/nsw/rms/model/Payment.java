package au.gov.nsw.rms.model;

import java.util.List;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Payments")
public class Payment
{
	private List<PaymentEntry> entries;

	public List<PaymentEntry> getEntries() {
		return entries;
	}
	
	public void addEntry(PaymentEntry newPaymentEntry){
		entries.add(newPaymentEntry);
	}

	@XmlElement(name = "Entry")
	public void setEntries(List<PaymentEntry> entries) {
		this.entries = entries;
	}
	
	public PaymentEntry getPaymentEntry(String pid){
		for (PaymentEntry paymentEntry : entries){
			if(paymentEntry.get_pid().equalsIgnoreCase(pid)){
				return paymentEntry;
			}
		}
		return null;
	}
}
