package au.gov.nsw.rms.model;

import javax.xml.bind.annotation.*;


@XmlRootElement(name="Entry")
public class RegistrationEntry
{
	private String rid;
	private Driver driver;
	private String regoNumber;
	private String regoValidity;
	
	public String getRid()
	{
		return rid;
	}

	@XmlElement(name="_rid")
	public void setRid(String rid)
	{
		this.rid = rid;
	}

	public Driver getDriver()
	{
		return driver;
	}

	@XmlElement(name="Driver")
	public void setDriver(Driver driver)
	{
		this.driver = driver;
	}

	public String getRegoNumber()
	{
		return regoNumber;
	}

	@XmlElement(name="RegistrationNumber")
	public void setRegoNumber(String regoNumber)
	{
		this.regoNumber = regoNumber;
	}

	public String getRegoValidity()
	{
		return regoValidity;
	}

	@XmlElement(name="RegistrationValidTill")
	public void setRegoValidity(String regoValidity)
	{
		this.regoValidity = regoValidity;
	}
	
}
