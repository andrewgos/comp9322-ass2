package au.gov.nsw.rms.model;


public class Customer
{
	private Car car;
	private String cid;
	private Driver driver;
	private String isCheckUpToDate;

	public String getCid()
	{
		return cid;
	}

	public void setCid(String cid)
	{
		this.cid = cid;
	}

	public Car getCar()
	{
		return car;
	}

	public void setCar(Car car)
	{
		this.car = car;
	}

	public Driver getDriver()
	{
		return driver;
	}

	public void setDriver(Driver driver)
	{
		this.driver = driver;
	}

	public String isCheckUpToDate()
	{
		return isCheckUpToDate;
	}

	public void setCheckUpToDate(String isCheckUpToDate)
	{
		this.isCheckUpToDate = isCheckUpToDate;
	}
	
}
