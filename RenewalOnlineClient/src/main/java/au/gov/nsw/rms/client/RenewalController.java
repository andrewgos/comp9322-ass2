package au.gov.nsw.rms.client;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import au.gov.nsw.rms.autocheck.AutoCheckResponse;
import au.gov.nsw.rms.autocheck.AutoCheckService;
import au.gov.nsw.rms.bean.Notice;
import au.gov.nsw.rms.greenslip.GreenSlipCheckRequest;
import au.gov.nsw.rms.service.EmailSender;


@Controller
@RequestMapping(value = "/officer")
public class RenewalController
{
	private static final String OFFICER_KEY = "ooo-123";
	private static final String REST_URI = "http://localhost:8080/ResourceOperationService";
	
	@Autowired
	private AutoCheckService autocheck;
	
	@RequestMapping(value="/index")
	public String generateRenewalNotices(ModelMap model, HttpServletRequest httpRequest)
			throws JsonProcessingException, IOException
	{
		Response response;
		ObjectMapper mapper = new ObjectMapper();
		WebClient client = WebClient.create(REST_URI);
		client.header("Key", OFFICER_KEY);
		List<Notice> notices = new LinkedList<Notice>();
		String action = httpRequest.getParameter("action");
		
		if(httpRequest.getMethod().equalsIgnoreCase("post") && action != null && action.equalsIgnoreCase("generate"))
		{
			client.path("/notice/generate").accept(MediaType.APPLICATION_JSON);
			response = client.post(null);
			
			if(response.getStatus() == Status.CREATED.getStatusCode())
			{
				JsonNode uris = mapper.readTree(response.readEntity(String.class)).get("uri");
				Iterator<JsonNode> urisIterator = uris.iterator();
				
				while(urisIterator.hasNext())
				{
					JsonNode node = urisIterator.next();
					String uri = node.get("uri").getTextValue();
					String rid = node.get("_rid").getTextValue();
					
					client.back(true);
					client.path("/car/" +rid).accept(MediaType.APPLICATION_JSON);
					response = client.get();

					JsonNode driverNode = mapper.readTree(response.readEntity(String.class)).get("driver");
					EmailSender email = new EmailSender(driverNode.get("email").getTextValue());
					email.send("RMS Renewal Notice", uri);
				}
				
				model.addAttribute("alert", uris.size() + " new notice(s) generated - email sent");
			}
			else
			{
				model.addAttribute("alert", "There are no new notice to generate");
			}
		}
		
		client.back(true);
		client.path("/notice").accept(MediaType.APPLICATION_JSON);
		response = client.get();
		
		if(response.getStatus() == Status.OK.getStatusCode())
		{
			JsonNode entries = mapper.readTree(response.readEntity(String.class)).get("entries");
			Iterator<JsonNode> entriesIterator = entries.iterator();
			
			while(entriesIterator.hasNext())
			{
				JsonNode node = entriesIterator.next();

				Notice notice = new Notice();
				notice.setNid(node.get("_nid").getTextValue());
				notice.setRid(node.get("_rid").getTextValue());
				notice.setStatus(node.get("status").getTextValue());
				
				notices.add(notice);
			}
		}
		
		Collections.sort(notices, new Notice());
		model.addAttribute("notices", notices);
		
		
		return "OfficerIndex";
	}

	@RequestMapping(value="/notice")
	public String getCarRegistration(ModelMap model, HttpServletRequest httpRequest)
			throws JsonProcessingException, IOException
	{
		String nid = httpRequest.getParameter("noticeId");
		
		if(nid != null)
		{
			Response response;
			String lastName = null, firstName = null, regoNumber = null;
			
			WebClient client = WebClient.create(REST_URI);
			client.header("Key", OFFICER_KEY);
			ObjectMapper mapper = new ObjectMapper();
			Notice notice = new Notice();
			
			// GET notice with id: nid
			client.path("/notice/" +nid).accept(MediaType.APPLICATION_JSON);
			response = client.get();
			String rid = null;
			
			if(response.getStatus() == Status.OK.getStatusCode())
			{
				JsonNode rootNode = mapper.readTree(response.readEntity(String.class));
				notice.setStatus(rootNode.get("status").getTextValue());
				notice.setRid(rootNode.get("_rid").getTextValue());
				notice.setNid(nid);
				
				rid = notice.getRid();
				model.addAttribute("noticeId", nid);
			}
			
			//GET car registration with id: rid
			client.back(true);
			client.path("/car/" +rid).accept(MediaType.APPLICATION_JSON);
			response = client.get();
			
			if(response.getStatus() == Status.OK.getStatusCode())
			{
				JsonNode rootNode = mapper.readTree(response.readEntity(String.class));
				regoNumber = rootNode.get("regoNumber").getTextValue();
				model.addAttribute("regoId", rootNode.get("rid").getTextValue());
				model.addAttribute("regoValidity", rootNode.get("regoValidity").getTextValue());
				model.addAttribute("regoNumber", regoNumber);
				
				JsonNode driverNode = rootNode.get("driver");
				lastName = driverNode.get("lastName").getTextValue();
				firstName = driverNode.get("firstName").getTextValue();
				model.addAttribute("driverEmail", driverNode.get("email").getTextValue());
				model.addAttribute("driverLicenseNumber", driverNode.get("driverLicenseNo").getTextValue());
				model.addAttribute("driverFirstName", firstName);
				model.addAttribute("driverLastName", lastName);
			}
			
			// Special Case - no field to store auto check response
			if(notice.getEnumStatus() == Notice.Status.UNDER_REVIEW)
			{
				GreenSlipCheckRequest request = new GreenSlipCheckRequest();
				request.setFirstName(firstName);
				request.setLastName(lastName);
				request.setRegoNumber(regoNumber);
				
				AutoCheckResponse checkResponse = autocheck.approve(request);
				model.addAttribute("checkApproved", checkResponse.getAccepted());
				model.addAttribute("checkDetails", checkResponse.getDetails());
			}
			
			// Submit button handler
			String action = httpRequest.getParameter("btnSubmit");
			if(httpRequest.getMethod().equalsIgnoreCase("post") && action != null)
			{
				if(action.equalsIgnoreCase("autoCheck"))
				{
					GreenSlipCheckRequest request = new GreenSlipCheckRequest();
					request.setFirstName(firstName);
					request.setLastName(lastName);
					request.setRegoNumber(regoNumber);
					
					AutoCheckResponse checkResponse = autocheck.approve(request);
					model.addAttribute("checkApproved", checkResponse.getAccepted());
					model.addAttribute("checkDetails", checkResponse.getDetails());
					
					client.back(true);
					client.path("/notice/" +nid +"/" +Notice.Status.UNDER_REVIEW);
					client.put(null);
					
					notice.setStatus(Notice.Status.UNDER_REVIEW.name());
				}
				else if(action.equalsIgnoreCase("accept"))
				{
					client.back(true);
					client.path("/notice/" +nid +"/" +Notice.Status.ACCEPTED);
					client.put(null);
					
					notice.setStatus(Notice.Status.ACCEPTED.name());
				}
				else if(action.equalsIgnoreCase("reject"))
				{
					client.back(true);
					client.path("/notice/" +nid +"/" +Notice.Status.REJECTED);
					client.put(null);
					
					notice.setStatus(Notice.Status.REJECTED.name());
				}
				else if(action.equalsIgnoreCase("setFee"))
				{
					client.back(true);
					client.path("/payment/create").type(MediaType.APPLICATION_FORM_URLENCODED);;
					Form form = new Form();
					form.param("nid", nid);
					form.param("rid", rid);
					form.param("amount", httpRequest.getParameter("txtAmount"));
					client.post(form);
					
					client.back(true);
					client.path("/notice/" +nid +"/" +Notice.Status.AWAITING_PAYMENT);
					client.put(null);

					notice.setStatus(Notice.Status.AWAITING_PAYMENT.name());
				}
			}
			
			model.addAttribute("noticeStatus", notice.getStatus());
		}
		
		
		return "OfficerNotice";
	}
	
}
