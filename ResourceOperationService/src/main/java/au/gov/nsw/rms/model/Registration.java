package au.gov.nsw.rms.model;

import java.util.List;
import javax.xml.bind.annotation.*;


@XmlRootElement(name="Registrations")
public class Registration
{
	private List<RegistrationEntry> entries;
	
	public List<RegistrationEntry> getEntries()
	{
		return entries;
	}
	
	@XmlElement(name="Entry")
	public void setEntries(List<RegistrationEntry> entries)
	{
		this.entries = entries;
	}
	
	public RegistrationEntry getEntry(String rid)
	{
		for (RegistrationEntry entry : entries)
		{
			if(entry.getRid().equalsIgnoreCase(rid))
				return entry;
		}
		
		return null;
	}
	
}
