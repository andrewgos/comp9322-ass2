package au.gov.nsw.rms.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Entry")
public class UriEntry {
	private String _rid;
    private String uri;
    
    
	public String get_rid() {
		return _rid;
	}
	@XmlElement(name="_rid")
	public void set_rid(String _rid) {
		this._rid = _rid;
	}
	public String getUri() {
		return uri;
	}	
	@XmlElement(name="uri")
	public void setUri(String uri) {
		this.uri = uri;
	}
	

}
