package au.gov.nsw.rms.resource;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import au.gov.nsw.rms.dao.CarDao;
import au.gov.nsw.rms.model.*;


@Path("/car")
public class CarResource
{
	private static final String DRIVER_KEY = "ddd-123";
	private static final String OFFICER_KEY = "ooo-123";
	
	private CarDao dao = new CarDao();
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response getCarDetails(
			@HeaderParam("Key") String headerKey,
			@PathParam("id") String rid)
	{
		Response response;
		
		if(headerKey != null && (headerKey.equals(OFFICER_KEY) || headerKey.equals(DRIVER_KEY)))
		{
			RegistrationEntry entry = dao.getInstance().getEntry(rid);
			
			if(entry != null)
				response = Response.ok(entry).build();
			else
				response = Response.status(Status.NOT_FOUND).build();
		}
		else
		{
			response = Response.status(Status.UNAUTHORIZED).build();
		}
		
		return response;
	}
	
	@PUT
	@Path("{id}/{day}/{month}/{year}")
	public Response putCarStatus(
			@HeaderParam("Key") String headerKey,
			@PathParam("id") String rid,
			@PathParam("day") String day,
			@PathParam("month") String month,
			@PathParam("year") String year)
	{
		Response response;
		
		if(headerKey != null && headerKey.equals(DRIVER_KEY))
		{
			Registration registration = dao.getInstance();
			RegistrationEntry entry = registration.getEntry(rid);
			
			if(entry != null)
			{
				String newValidity = String.format("%s/%s/%s", day, month, year);
				
				if(entry.getRegoValidity().trim().equals(newValidity))
				{
					response = Response.status(Status.NOT_MODIFIED).build();
				}
				else
				{
					registration.getEntry(rid).setRegoValidity(newValidity);
					
					dao.saveCar(registration);
					response = Response.status(Status.OK).build();
				}
			}
			else
			{
				response = Response.status(Status.NOT_FOUND).build();
			}
			
		}
		else
		{
			response = Response.status(Status.UNAUTHORIZED).build();
		}
		
		return response;
	}
}
