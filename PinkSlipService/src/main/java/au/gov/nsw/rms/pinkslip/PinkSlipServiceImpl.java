package au.gov.nsw.rms.pinkslip;

import java.io.*;
import java.math.BigInteger;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.jws.WebService;
import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.gov.nsw.rms.model.*;
import au.gov.nsw.rms.pinkslipdefinition.*;


@WebService(endpointInterface="au.gov.nsw.rms.pinkslip.PinkSlipService")
public class PinkSlipServiceImpl implements PinkSlipService
{
	private Provider provider;
	private ObjectFactory factory;
	
	@Override
	public PinkSlipCheckOutputType pinkSlipCheck(PinkSlipInputType parameters) throws PinkSlipCheckFaultMsg
	{
		String lastName = parameters.getLastName();
		String firstName = parameters.getFirstName();
		String regoNumber = parameters.getRegoNumber();
		
		Customer customer = provider.findCustomer(firstName, lastName, regoNumber);
		
		if(customer == null)
		{
    		String code = "Driver not found";
    		String msg = "The driver with the details specified could not be found in our database";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new PinkSlipCheckFaultMsg(msg, fault);
		}
		
    	PinkSlipCheckOutputType response = factory.createPinkSlipCheckOutputType();
    	response.setLastName(lastName);
    	response.setFirstName(firstName);
    	response.setRegoNumber(regoNumber);
		response.setCheckedFlag(customer.isCheckUpToDate());
		
    	return response;
	}
	
	@Override
	public VehicleAgeOutputType vehicleAge(PinkSlipInputType parameters) throws VehicleAgeFaultMsg
	{
		String lastName = parameters.getLastName();
		String firstName = parameters.getFirstName();
		String regoNumber = parameters.getRegoNumber();
		
		Customer customer = provider.findCustomer(firstName, lastName, regoNumber);
		
		if(customer == null)
		{
    		String code = "Driver not found";
    		String msg = "The driver with the details specified could not be found in our database";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new VehicleAgeFaultMsg(msg, fault);
		}
		
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int vehicleYear = Integer.parseInt(customer.getCar().getYear());
		VehicleAgeOutputType response = factory.createVehicleAgeOutputType();
		response.setAge(BigInteger.valueOf(currentYear - vehicleYear));
		
		return response;
	}
	
	@PostConstruct
	private void initialiseProperties() throws ParserConfigurationException, SAXException, IOException
	{
		InputSource xmlFile = new InputSource(getClass().getClassLoader().getResourceAsStream("xml/PinkSlip_Database.xml"));
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(xmlFile);
		doc.getDocumentElement().normalize();
		
		provider = new Provider(doc);
		factory = new ObjectFactory();
	}
	
}
