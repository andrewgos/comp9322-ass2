package au.gov.nsw.rms.greenslip;

import java.io.IOException;

import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.gov.nsw.rms.greenslip.model.Entry;
import au.gov.nsw.rms.greenslip.model.ModelDatabase;


@WebService(endpointInterface = "au.gov.nsw.rms.greenslip.GreenSlipService")
public class GreenSlipServiceImpl implements GreenSlipService {
	private ObjectFactory factory = new ObjectFactory();
	private ModelDatabase modelData;

	@Override
	public GreenSlipCheckResponse greenSlipCheck(GreenSlipCheckRequest parameters)
			throws GreenSlipCheckFaultMsg {
		String firstName = parameters.getFirstName();
		String lastName = parameters.getLastName();
		String regoNumber = parameters.getRegoNumber();
		
		modelData = new ModelDatabase();
		try {
			InputSource xmlFile = new InputSource(getClass().getClassLoader().getResourceAsStream("xml/GreenSlip_Database.xml"));
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(xmlFile);
			doc.getDocumentElement().normalize();
	        modelData.parseDocument(doc);
	        System.out.println(doc);
	        
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		
		
		Entry customer = modelData.getCustomer(firstName, lastName, regoNumber);
		
		if(firstName == null || lastName == null || regoNumber == null) {
			String code = "Invalid argument";
    		String msg = "All the fields must not be empty";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new GreenSlipCheckFaultMsg(msg, fault);
		}
		if(customer == null) {
			String code = "Invalid content";
    		String msg = "Driver not found";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new GreenSlipCheckFaultMsg(msg, fault);
		}
		
		GreenSlipCheckResponse response = factory.createGreenSlipCheckResponse();
		response.setLastName(customer.getDriver().getLastName());
    	response.setFirstName(customer.getDriver().getFirstName());
    	response.setRegoNumber(customer.getRegoNumber());
		response.setPaidFlag(customer.getInsurance().getCurrentlyValid());
		
		return response;
	}
}
