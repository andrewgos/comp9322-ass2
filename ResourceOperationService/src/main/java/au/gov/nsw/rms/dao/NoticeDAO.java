package au.gov.nsw.rms.dao;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import au.gov.nsw.rms.model.Notice;
import au.gov.nsw.rms.model.NoticeEntry;

public class NoticeDAO {
	private static final String XML_PATH = "src/main/resources/xml/Renewal_Notices.xml";

    public static Notice getNotice(){
    	// UNMARSHALLING XML INTO OBJECT
		Notice notice = null;
		File xmlFile = new File(XML_PATH);
		try {
			JAXBContext context = JAXBContext.newInstance(Notice.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			notice = (Notice) unmarshaller.unmarshal(xmlFile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return notice;
    }
    
    public static void setNotice(Notice newNotice){
    	// MARSHALLING OBJECT INTO XML
		File xmlFile = new File(XML_PATH);
		try {
			JAXBContext context = JAXBContext.newInstance(Notice.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(newNotice, xmlFile);
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
    }
    
    public static void updateNotice(String nid, String newStatus){
    	// CHANGE STATUS OF A NOTICE IN THE XML
    	Notice notice = getNotice();
    	
		for(int i = 0 ; i<notice.getEntries().size();i++){
    		if(notice.getEntries().get(i).get_nid().equals(nid)){
    			notice.getEntries().get(i).setStatus(newStatus);
    			break;
    		}
    	}
		setNotice(notice);
    }
    
    public static void deleteNotice(String nid){
    	// DELETE A NOTICE FROM XML
    	Notice notice = getNotice();
    	
		for(int i = 0 ; i<notice.getEntries().size();i++){
    		if(notice.getEntries().get(i).get_nid().equals(nid)){
    			notice.getEntries().remove(i);
    			break;
    		}
    	}
		setNotice(notice);
    }
    
    public static String getNoticeStatus(String nid){
    	Notice notice = getNotice();
    	NoticeEntry noticeEntry = notice.getNoticeEntry(nid);
    	if(noticeEntry != null){
    		return noticeEntry.getStatus();
    	}
    	return null;
    }
    

    
    public static boolean isNoticeActive(String rid, String date){
    	Notice notice = getNotice();
    	String formattedDate = date.replace("/", "");
    	
    	for(int i = 0 ; i<notice.getEntries().size();i++){
    		if(notice.getEntries().get(i).get_rid().equals(rid)){
    			if(notice.getEntries().get(i).get_nid().substring(0, 8).replace("/", "").equals(formattedDate)){
    				
    				if(notice.getEntries().get(i).getStatus().equals(Notice.Status.CREATED.name()) ||
						notice.getEntries().get(i).getStatus().equals(Notice.Status.REQUESTED.name()) ||
						notice.getEntries().get(i).getStatus().equals(Notice.Status.UNDER_REVIEW.name()) ||
						notice.getEntries().get(i).getStatus().equals(Notice.Status.ACCEPTED.name()) ||
						notice.getEntries().get(i).getStatus().equals(Notice.Status.AWAITING_PAYMENT.name()) ){
        				return true;
        			}
    			}
    		}
    	}
		return false;
    	
    }
    
    public static int getNoticeIDCounter(String rid, String date){
    	Notice notice = getNotice();
    	int counter = 0;
    	String formattedDate = date.replace("/", "");
    	
    	for(int i = 0 ; i<notice.getEntries().size();i++){
    		if(notice.getEntries().get(i).get_rid().equals(rid)){
    			if(notice.getEntries().get(i).get_nid().substring(0, 8).replace("/", "").equals(formattedDate)){
    				counter++;
    			}
    			
    		}
    	}
		return counter;
    }
    
    
    
}
