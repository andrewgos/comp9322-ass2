package au.gov.nsw.rms.bean;

import java.util.Comparator;


public class Notice implements Comparator<Notice>
{
	private String nid;
	private String rid;
	private Status status;
	
	public String getNid()
	{
		return nid;
	}
	
	public void setNid(String nid)
	{
		this.nid = nid;
	}

	public String getRid()
	{
		return rid;
	}
	
	public void setRid(String rid)
	{
		this.rid = rid;
	}
	
	public String getStatus()
	{
		return status.name();
	}
	
	public Status getEnumStatus()
	{
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = Status.valueOf(status.toUpperCase());
	}

	@Override
	public int compare(Notice o1, Notice o2)
	{
		return o1.status.compareTo(o2.status);
	}

	public enum Status
	{
		CREATED,
		REQUESTED,
		UNDER_REVIEW,
		ACCEPTED,
		REJECTED,
		AWAITING_PAYMENT,
		CANCELLED,
		ARCHIVED,
		COMPLETED
	}
	
}

