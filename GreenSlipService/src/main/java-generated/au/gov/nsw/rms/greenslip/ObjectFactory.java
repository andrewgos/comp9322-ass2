
package au.gov.nsw.rms.greenslip;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.gov.nsw.rms.greenslip package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GreenSlipCheckFault_QNAME = new QName("http://greenslip.rms.nsw.gov.au", "greenSlipCheckFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.gov.nsw.rms.greenslip
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GreenSlipCheckRequest }
     * 
     */
    public GreenSlipCheckRequest createGreenSlipCheckRequest() {
        return new GreenSlipCheckRequest();
    }

    /**
     * Create an instance of {@link GreenSlipCheckResponse }
     * 
     */
    public GreenSlipCheckResponse createGreenSlipCheckResponse() {
        return new GreenSlipCheckResponse();
    }

    /**
     * Create an instance of {@link ServiceFaultType }
     * 
     */
    public ServiceFaultType createServiceFaultType() {
        return new ServiceFaultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://greenslip.rms.nsw.gov.au", name = "greenSlipCheckFault")
    public JAXBElement<ServiceFaultType> createGreenSlipCheckFault(ServiceFaultType value) {
        return new JAXBElement<ServiceFaultType>(_GreenSlipCheckFault_QNAME, ServiceFaultType.class, null, value);
    }

}
