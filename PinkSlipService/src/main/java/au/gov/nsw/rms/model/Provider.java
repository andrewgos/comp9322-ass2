package au.gov.nsw.rms.model;

import java.util.*;
import org.w3c.dom.*;


public class Provider
{
	private LinkedList<Customer> customers;
	
	public Provider(Document doc)
	{
		customers = new LinkedList<Customer>();
		populateCustomers(doc);
	}
	
	public Customer findCustomer(String firstName, String lastName, String rego)
	{
		for (Customer customer : customers)
		{
			if(customer.getCar().getRego().equalsIgnoreCase(rego.trim()) &&
				customer.getDriver().getLastName().equalsIgnoreCase(lastName.trim()) &&
				customer.getDriver().getFirstName().equalsIgnoreCase(firstName.trim()))
				return customer;
		}
		
		return null;
	}
	
	private void populateCustomers(Document doc)
	{
		NodeList entries = doc.getElementsByTagName("Entry");
		
		for(int i = 0; i < entries.getLength(); i++)
		{
			Customer customer = new Customer();
			NodeList entryElements = entries.item(i).getChildNodes();
			
			for(int j = 0; j < entryElements.getLength(); j++)
			{
				Node e = entryElements.item(j);
				
				if(e.getNodeName().equalsIgnoreCase("_cid"))
				{
					customer.setCid(e.getTextContent());
				}
				else if(e.getNodeName().equalsIgnoreCase("SafetyCheckUpToDate"))
				{
					customer.setCheckUpToDate(e.getTextContent());
				}
				else if(e.getNodeName().equalsIgnoreCase("Driver"))
				{
					Driver driver = new Driver();
					NodeList driverElements = e.getChildNodes();
					
					for(int k = 0; k < driverElements.getLength(); k++)
					{
						Node d = driverElements.item(k);
						
						if(d.getNodeName().equalsIgnoreCase("LastName"))
						{
							driver.setLastName(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("FirstName"))
						{
							driver.setFirstName(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("DriversLicenseNo"))
						{
							driver.setLicenseNo(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("Email"))
						{
							driver.setEmail(d.getTextContent());
						}
					}
					
					customer.setDriver(driver);
				}
				else if(e.getNodeName().equalsIgnoreCase("CarDetails"))
				{
					Car car = new Car();
					NodeList carElements = e.getChildNodes();
					
					for(int k = 0; k < carElements.getLength(); k++)
					{
						Node c = carElements.item(k);
						
						if(c.getNodeName().equalsIgnoreCase("RegistrationNumber"))
						{
							car.setRego(c.getTextContent());
						}
						else if(c.getNodeName().equalsIgnoreCase("ManufacturedYear"))
						{
							car.setYear(c.getTextContent());
						}
					}
					
					customer.setCar(car);
				}
			}
			
			customers.add(customer);
		}
	}

}
