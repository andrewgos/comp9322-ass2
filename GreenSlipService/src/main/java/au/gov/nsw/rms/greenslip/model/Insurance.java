package au.gov.nsw.rms.greenslip.model;

public class Insurance {
	private String insuranceNumber;
	private String currentlyValid;
	
	public String getInsuranceNumber() {
		return insuranceNumber;
	}
	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}
	public String getCurrentlyValid() {
		return currentlyValid;
	}
	public void setCurrentlyValid(String currentlyValid) {
		this.currentlyValid = currentlyValid;
	}

}
