package au.gov.nsw.rms.model;


public class Car
{
	private String rego;
	private String year;
	
	public String getRego()
	{
		return rego;
	}

	public void setRego(String rego)
	{
		this.rego = rego;
	}

	public String getYear()
	{
		return year;
	}

	public void setYear(String year)
	{
		this.year = year;
	}

}
