package au.gov.nsw.rms.greenslip.model;

import java.util.ArrayList;

import org.w3c.dom.*;


public class ModelDatabase {
	
	private ArrayList<Entry> entryList;
	
	public ModelDatabase() {
		entryList = new ArrayList<Entry>();
	}
	
	public void parseDocument(Document doc) {
		NodeList entries = doc.getElementsByTagName("Entry");
		
		for(int i=0; i < entries.getLength(); i++) {
			Entry newEntry = new Entry();
			
			Node currEntryNode = entries.item(i);
			NodeList entryElements = currEntryNode.getChildNodes();
			
			for (int j=0; j < entryElements.getLength(); j++ ) {
				Node e = entryElements.item(j);
				
				if(e.getNodeName().equalsIgnoreCase("_cid")) {
					newEntry.set_cid(e.getTextContent());
				}
				else if(e.getNodeName().equalsIgnoreCase("RegistrationNumber")) {
					newEntry.setRegoNumber(e.getTextContent());
				}
				else if(e.getNodeName().equalsIgnoreCase("Driver")) {
					Driver newDriver = new Driver();
					NodeList driverElements = e.getChildNodes();
					
					for (int k=0; k < driverElements.getLength(); k++ ) {
						Node d = driverElements.item(k);
						
						if(d.getNodeName().equalsIgnoreCase("LastName")) {
							newDriver.setLastName(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("FirstName")) {
							newDriver.setFirstName(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("DriversLicenseNo")){
							newDriver.setLicenseNo(d.getTextContent());
						}
						else if(d.getNodeName().equalsIgnoreCase("Email")){
							newDriver.setEmail(d.getTextContent());
						}
					}
					newEntry.setDriver(newDriver);
				}
				else if(e.getNodeName().equalsIgnoreCase("CTPDetails")){
					Insurance newInsurance = new Insurance();
					NodeList insuranceElements = e.getChildNodes();
					
					for (int k=0; k < insuranceElements.getLength(); k++ ){
						Node ins = insuranceElements.item(k);
						
						if(ins.getNodeName().equalsIgnoreCase("InsurancePolicyNo")){
							newInsurance.setInsuranceNumber(ins.getTextContent());
						}
						else if(ins.getNodeName().equalsIgnoreCase("CurrentlyValid")){
							newInsurance.setCurrentlyValid(ins.getTextContent());
						}	
					}
					newEntry.setInsurance(newInsurance);
				}
			}
			entryList.add(newEntry);
		}
	}
	
	public Entry getCustomer(String firstName, String lastName, String regoNumber){
		//System.out.println(lastName +firstName + regoNumber);
		//System.out.println(entryList.size());
		for (Entry entry : entryList){
			if(entry.getRegoNumber().equalsIgnoreCase(regoNumber) &&
				entry.getDriver().getLastName().equalsIgnoreCase(lastName) &&
				entry.getDriver().getFirstName().equalsIgnoreCase(firstName)){
				return entry;
			}
		}
		return null;
	}
}
