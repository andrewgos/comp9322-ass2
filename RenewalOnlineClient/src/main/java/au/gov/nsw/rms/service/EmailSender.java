package au.gov.nsw.rms.service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;


public class EmailSender
{
    private String to;
	
	public EmailSender(String to)
	{
		this.to = to;
	}
	
	public void send(String subject, String content)
	{
		final String username = "rms.comp.9322@gmail.com";
		final String password = "comp9322ass2";
		
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.port", "587");
		Session session = Session.getDefaultInstance(properties, new Authenticator()
			{
				protected PasswordAuthentication getPasswordAuthentication()
				{
					return new PasswordAuthentication(username, password);
				}
			}
		);
		
		try
		{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(content);
			
			Transport.send(message);
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}
	}
	
}
