<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<title>RMS Online Client - Officer Dashboard</title>
</head>
<body>
	<div class="container">
		<c:if test="${not empty alert}">
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<c:out value="${alert}" />
		</div>
		</c:if>
		
		<div class="page-header">
			<h1>RMS Renewal Online System</h1>
		</div>
		
		<div class="row">
			<div class="col-sm-1">
				<form method="post" action="index">
					<button type="submit" name="action" class="btn btn-info" value="generate">Generate Renewal Notices</button>
				</form>
			</div>
		</div>
		
		<br/>
		<br/>
		
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Notice Id</th>
					<th>Registration Id</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${notices}" var="notice">
				<tr>
					<td><c:out value="${notice.nid}" /></td>
					<td><c:out value="${notice.rid}" /></td>
					<td><c:out value="${notice.status}" /></td>
					<td>
						<form action="notice">
							<button type="submit" name="noticeId" class="btn btn-primary" value="${notice.nid}">Select</button>
						</form>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>