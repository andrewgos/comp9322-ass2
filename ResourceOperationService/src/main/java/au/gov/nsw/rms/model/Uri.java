package au.gov.nsw.rms.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "NoticeURIs")
public class Uri{
	
	private List<UriEntry> entries = new ArrayList<UriEntry>();

	public List<UriEntry> getUri() {
		return entries;
	}
	
	
	@XmlElement(name = "Entry")
	public void setUri(List<UriEntry> uris) {
		this.entries = uris;
	}

	public void addUriEntry(UriEntry uri){
		entries.add(uri);
	}

}
