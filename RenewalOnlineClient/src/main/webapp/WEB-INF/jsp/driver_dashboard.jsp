<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<title>RMS Car Registration | Driver</title>


</head>
<body>




	<div class="container">
	
		<c:if test="${not empty alert}">
			<div class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<c:out value="${alert}" />
			</div>
		</c:if>

		<div class="page-header">
			<h1>RMS Renewal Online System</h1>
		</div>
		
		
		
		
		<ul class="nav nav-tabs" style="width:500px">
			<li class="active"><a href="#pane1" data-toggle="tab">Renewal Notice Details</a></li>
			<li><a href="#pane2" data-toggle="tab">Registration Details</a></li>
			<li><a href="#pane3" data-toggle="tab">Driver Details</a></li>
		</ul>

		<div class="tab-content" style="width:500px">
		
			<div id="pane1" class="tab-pane active">
				<table class="table">
					<tr>
						<td><strong>Notice ID</strong></td>
						<td>${_nid}</td>
					</tr>
					<tr>
						<td><strong>Registration ID</strong></td>
						<td>${_rid}</td>
					<tr>
						<td><strong>Status</strong></td>
						<td>${status}</td>
					</tr>
				</table>
				
				<c:choose>
					<c:when test="${status eq 'CREATED'}">
						<form action="update">
							<button type="submit" class="btn btn-primary" name="action" value="request">Request</button>
						</form>
					</c:when>
					
					<c:when test="${status eq 'REQUESTED'}">
						<form action="update">
							<button type="submit" class="btn btn-primary" name="action" value="cancel">Cancel</button>
						</form>
					</c:when>
					
					<c:when test="${status eq 'UNDER_REVIEW'}">
						Please wait while our officer is reviewing your request.
					</c:when>
					
					<c:when test="${status eq 'ACCEPTED'}">
						Please wait while our officer is setting the fee amount.
					</c:when>
					
					<c:when test="${status eq 'AWAITING_PAYMENT'}">
						<div id="visible">
							<button class="btn btn-primary" onclick="showForm();">Pay</button>
						</div>
						<div id="paymentform" style="visibility: hidden">
							<br>
							<table class="table">
								<tr>
									<th colspan="2">Your Payment Details:</th>
								</tr>
								<tr>
									<td><strong>Payment ID</strong></td>
									<td>${_pid}</td>
								<tr>
									<td><strong>Amount</strong></td>
									<td>$${amount}</td>
								</tr>
							</table>
							<form action="pay" method="POST">
								<label class="control-label">Credit Card Details:</label><br>
								<input type="text" class="form-control" name="creditcarddetail" placeholder="XXXX-XXXX-XXXX-XXXX">
								<input type="hidden" name="regovalidity" value="${regoValidity}"><br>
								<button type="submit" class="btn btn-primary">Check Out</button>
							</form>
						</div>
					</c:when>
					
					<c:when test="${status eq 'COMPLETED'}">
						Congratulations! You registration has been renewed.
					</c:when>
					
					<c:when test="${status eq 'ARCHIVED'}">
						This renewal notice has been archived.
					</c:when>
					
					<c:when test="${status eq 'REJECTED'}">
						<form action="update">
							<button type="submit" class="btn btn-primary" name="action" value="archive">Archive</button>
						</form>
					</c:when>
					
					<c:when test="${status eq 'CANCELLED'}">
						<form action="update">
							<button type="submit" class="btn btn-primary" name="action" value="archive">Archive</button>
						</form>
					</c:when>
				</c:choose>
			
				
			</div>
			<div id="pane2" class="tab-pane">
				<table class="table">
					<tr>
						<td><strong>Registration ID</strong></td>
						<td>${_rid}</td>
					</tr>
					<tr>
						<td><strong>Registration Number</strong></td>
						<td>${regoNumber}</td>
					<tr>
						<td><strong>Registration Valid Until</strong></td>
						<td>${regoValidity}</td>
					</tr>

	
				</table>
			</div>
			
			<div id="pane3" class="tab-pane">
				<table class="table">
					<tr>
						<td><strong>Last Name</strong></td>
						<td>${lastName}</td>
					</tr>
					<tr>
						<td><strong>First Name</strong></td>
						<td>${firstName}</td>
					</tr>
					<tr>
						<td><strong>Email</strong></td>
						<td>${email}</td>
					</tr>
					<tr>
						<td><strong>Driver License Number</strong></td>
						<td>${driverLicenseNo}</td>
					</tr>
				</table>
			</div>
			
			
		</div>

 <%-- 
		<div class="row">
			<div class="col-xs-6">
				<div class="well">
					
					
				</div> <!-- end of well -->
			</div> <!-- end of col -->
			<div class="col-xs-6">
				<div class="well">
					

					
				</div> <!-- end of well -->
			</div> <!-- end of col -->

			
		</div> <!-- end of row -->









		<div class="row">
			<div class="col-xs-5">
				<div class="well">
					
				</div> <!-- end of well -->
			</div> <!-- end of col -->
			<div class="col-xs-5">
				<div class="well">
					
				</div> <!-- end of well -->
			</div> <!-- end of col -->
			
		</div> <!-- end of row -->
		
		


  --%>



	</div> <!-- end of container -->


<script>
	function showForm() {
		var visible
		var hidden
		visible = document.getElementById("visible").innerHTML;
		hidden = document.getElementById("paymentform").innerHTML;
		document.getElementById("visible").innerHTML = hidden;
	}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


</body>
</html>