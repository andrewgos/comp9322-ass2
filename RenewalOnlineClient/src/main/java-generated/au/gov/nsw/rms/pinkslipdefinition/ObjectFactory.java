
package au.gov.nsw.rms.pinkslipdefinition;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the au.gov.nsw.rms.pinkslipdefinition package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PinkSlipCheckFault_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "pinkSlipCheckFault");
    private final static QName _PinkSlipCheckRequest_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "pinkSlipCheckRequest");
    private final static QName _PinkSlipCheckResponse_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "pinkSlipCheckResponse");
    private final static QName _VehicleAgeFault_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "vehicleAgeFault");
    private final static QName _VehicleAgeRequest_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "vehicleAgeRequest");
    private final static QName _VehicleAgeResponse_QNAME = new QName("http://rms.nsw.gov.au/pinkslipdefinition", "vehicleAgeResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: au.gov.nsw.rms.pinkslipdefinition
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceFaultType }
     * 
     */
    public ServiceFaultType createServiceFaultType() {
        return new ServiceFaultType();
    }

    /**
     * Create an instance of {@link PinkSlipInputType }
     * 
     */
    public PinkSlipInputType createPinkSlipInputType() {
        return new PinkSlipInputType();
    }

    /**
     * Create an instance of {@link PinkSlipCheckOutputType }
     * 
     */
    public PinkSlipCheckOutputType createPinkSlipCheckOutputType() {
        return new PinkSlipCheckOutputType();
    }

    /**
     * Create an instance of {@link VehicleAgeOutputType }
     * 
     */
    public VehicleAgeOutputType createVehicleAgeOutputType() {
        return new VehicleAgeOutputType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "pinkSlipCheckFault")
    public JAXBElement<ServiceFaultType> createPinkSlipCheckFault(ServiceFaultType value) {
        return new JAXBElement<ServiceFaultType>(_PinkSlipCheckFault_QNAME, ServiceFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PinkSlipInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "pinkSlipCheckRequest")
    public JAXBElement<PinkSlipInputType> createPinkSlipCheckRequest(PinkSlipInputType value) {
        return new JAXBElement<PinkSlipInputType>(_PinkSlipCheckRequest_QNAME, PinkSlipInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PinkSlipCheckOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "pinkSlipCheckResponse")
    public JAXBElement<PinkSlipCheckOutputType> createPinkSlipCheckResponse(PinkSlipCheckOutputType value) {
        return new JAXBElement<PinkSlipCheckOutputType>(_PinkSlipCheckResponse_QNAME, PinkSlipCheckOutputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "vehicleAgeFault")
    public JAXBElement<ServiceFaultType> createVehicleAgeFault(ServiceFaultType value) {
        return new JAXBElement<ServiceFaultType>(_VehicleAgeFault_QNAME, ServiceFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PinkSlipInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "vehicleAgeRequest")
    public JAXBElement<PinkSlipInputType> createVehicleAgeRequest(PinkSlipInputType value) {
        return new JAXBElement<PinkSlipInputType>(_VehicleAgeRequest_QNAME, PinkSlipInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VehicleAgeOutputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rms.nsw.gov.au/pinkslipdefinition", name = "vehicleAgeResponse")
    public JAXBElement<VehicleAgeOutputType> createVehicleAgeResponse(VehicleAgeOutputType value) {
        return new JAXBElement<VehicleAgeOutputType>(_VehicleAgeResponse_QNAME, VehicleAgeOutputType.class, null, value);
    }

}
