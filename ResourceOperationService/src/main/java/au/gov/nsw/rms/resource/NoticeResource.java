package au.gov.nsw.rms.resource;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import au.gov.nsw.rms.dao.CarDao;
import au.gov.nsw.rms.dao.NoticeDAO;
import au.gov.nsw.rms.model.Notice;
import au.gov.nsw.rms.model.NoticeEntry;
import au.gov.nsw.rms.model.Uri;
import au.gov.nsw.rms.model.Registration;
import au.gov.nsw.rms.model.RegistrationEntry;
import au.gov.nsw.rms.model.UriEntry;

@Path("/notice")
public class NoticeResource {
	
	private static final String DRIVER_KEY = "ddd-123";
	private static final String OFFICER_KEY = "ooo-123";


    @POST
    @Path("/generate/")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response generateAllNotices(@HeaderParam("Key") String headerKey) {
    	// AUTHENTICATION CHECK
    	if(headerKey == null || !headerKey.equals(OFFICER_KEY)){
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;
    	}
    	
    	Uri newNoticeURI = new Uri();
    	
    	// GETTING CAR REGISTRATION OBJECT
    	Registration registration = new CarDao().getInstance();
		// GETTING NOTICE OBJECT
		Notice notice = NoticeDAO.getNotice();
		
		// CHECK EVERY REGISTRATION
		for(RegistrationEntry registrationEntry : registration.getEntries()){
			Date expiredDate = new Date();
			Date currDate = new Date();
			
			// PARSING THE EXPIRED DATE
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			try {
				expiredDate = dateFormat.parse(registrationEntry.getRegoValidity());
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

			// GET EXPIRED DATE MINUS 1 MONTH
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(expiredDate);
			calendar.add(Calendar.MONTH, -1);
			Date oneMonthBeforeExpiredDate = calendar.getTime();
			
			// CHECK THE DATE
			if(currDate.after(oneMonthBeforeExpiredDate) && currDate.before(expiredDate)) {
				
				// SKIP THIS IF STILL ACTIVE
				if(NoticeDAO.isNoticeActive(registrationEntry.getRid(), registrationEntry.getRegoValidity())){
					continue;
				}
				// GET CURRENT NID COUNTER FOR THIS DRIVER
				int counter = NoticeDAO.getNoticeIDCounter(registrationEntry.getRid(), registrationEntry.getRegoValidity());
				String regoDate = registrationEntry.getRegoValidity().replace("/", "");
				String newNid = regoDate+String.format("%03d", counter);
				
				// GENERATE NEW NOTICE
				NoticeEntry newNoticeEntry = new NoticeEntry();
				newNoticeEntry.set_nid(newNid);
				newNoticeEntry.set_rid(registrationEntry.getRid());
				newNoticeEntry.setStatus(Notice.Status.CREATED.name());
				notice.addEntry(newNoticeEntry);
				
		    	// WRITING INTO XML
				NoticeDAO.setNotice(notice);
				
				// ADD INTO RESPONSE XML
				UriEntry newUriEntry = new UriEntry();
				newUriEntry.set_rid(newNoticeEntry.get_rid());
				newUriEntry.setUri("http://localhost:8080/RenewalOnlineClient/driver/"+newNoticeEntry.get_nid());
				newNoticeURI.addUriEntry(newUriEntry);
			}
		}
		
		// IF NOTHING GENERATED
     	if(newNoticeURI.getUri().size() == 0){
     		Response response = Response.status(Status.NOT_MODIFIED).build();
        	return response;
     	}
		
    	Response response = Response.status(Status.CREATED).entity(newNoticeURI).build();
    	return response;
    }
    
    @GET
	@Path("{nid}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response getNoticeDetail(@PathParam("nid") String nid, @HeaderParam("Key") String headerKey) {
    	// AUTHENTICATION CHECK
    	if(headerKey != null &&( headerKey.equals(OFFICER_KEY) || headerKey.equals(DRIVER_KEY))){
    		// GET NOTICE ENTRY FOR DETAILS
    		NoticeEntry noticeEntry = NoticeDAO.getNotice().getNoticeEntry(nid);
        	if(noticeEntry == null){
        		Response response = Response.status(Status.NOT_FOUND).build();
            	return response;
        	}
        	Response response = Response.status(Status.OK).entity(noticeEntry).build();
        	return response;
    	}
    	else{
    		// UNAUTHORIZED
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;	
    	}
    	
    	
	}
    
    @GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response getAllNoticeDetail(@HeaderParam("Key") String headerKey) {
    	// AUTHENTICATION CHECK
    	if(headerKey == null || !headerKey.equals(OFFICER_KEY)){
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;
    	}
    	// GET ALL NOTICE
		Notice notice = NoticeDAO.getNotice();
    	Response response = Response.status(Status.OK).entity(notice).build();
    	return response;
	}
    
    @PUT
    @Path("{nid}/{status}")
	public Response updateNotice(@PathParam("nid") String nid, @PathParam("status") String newStatus, @HeaderParam("Key") String headerKey) {
    	Notice notice = NoticeDAO.getNotice();
    	
    	// AUTHENTICATION CHECK
    	if(headerKey == null || (!headerKey.equals(DRIVER_KEY) && !headerKey.equals(OFFICER_KEY))){
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;
    	}
    	if(headerKey.equals(OFFICER_KEY)){
    		if(!newStatus.equals(Notice.Status.UNDER_REVIEW.name())&&
				!newStatus.equals(Notice.Status.ACCEPTED.name())&&
				!newStatus.equals(Notice.Status.REJECTED.name())&&
				!newStatus.equals(Notice.Status.AWAITING_PAYMENT.name())){
    			Response response = Response.status(Status.UNAUTHORIZED).build();
            	return response;
    		}
    	}
    	if(headerKey.equals(DRIVER_KEY)){
    		if(!newStatus.equals(Notice.Status.REQUESTED.name())&&
				!newStatus.equals(Notice.Status.CANCELLED.name())&&
				!newStatus.equals(Notice.Status.ARCHIVED.name())&&
				!newStatus.equals(Notice.Status.COMPLETED.name())){
    			System.out.println(Notice.Status.CANCELLED.name());
    			Response response = Response.status(Status.UNAUTHORIZED).build();
            	return response;
    		}
    	}
    	
    	
    	
    	// CHECK CERTAIN STATUS CAN ONLY CHANGE CERTAIN STATUS
    	if(newStatus.equals(Notice.Status.REQUESTED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.CREATED.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.CANCELLED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.REQUESTED.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.UNDER_REVIEW.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.REQUESTED.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.ACCEPTED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.UNDER_REVIEW.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.REJECTED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.UNDER_REVIEW.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.AWAITING_PAYMENT.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.ACCEPTED.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	if(newStatus.equals(Notice.Status.COMPLETED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.AWAITING_PAYMENT.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}
    	
    	if(newStatus.equals(Notice.Status.ARCHIVED.name())){
    		if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.REJECTED.name())&&
    			!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.CANCELLED.name())){
    			Response response = Response.status(Status.BAD_REQUEST).build();
            	return response;
        	}
    	}

    	
    	
    	
    	// IF IT IS ALREADY THE SAME STATUS
    	if(notice.getNoticeEntry(nid).getStatus().equals(newStatus)){
    		Response response = Response.status(Status.NOT_MODIFIED).build();
    		return response;
    	}
    	
    	NoticeDAO.updateNotice(nid, newStatus);
    	Response response = Response.status(Status.OK).build();
		return response;

	}
    
    
    @DELETE
	@Path("{nid}")
	public Response deleteNotice(@PathParam("nid") String nid, @HeaderParam("Key") String headerKey) {
    	// AUTHENTICATION CHECK
    	if(headerKey == null || !headerKey.equals(DRIVER_KEY)){
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;
    	}
    	
    	Notice notice = NoticeDAO.getNotice();
    	
    	// CHECK CAN ONLY ARCHIVE IF REJECTED OR CANCELLED
    	if(!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.REJECTED.name())&&
			!notice.getNoticeEntry(nid).getStatus().equals(Notice.Status.CANCELLED.name())){
			Response response = Response.status(Status.BAD_REQUEST).build();
        	return response;
    	}
		
    	NoticeDAO.updateNotice(nid, Notice.Status.ARCHIVED.name());
    	Response response = Response.status(Status.OK).build();
    	return response;
	}
    
    
    
    
    

}

