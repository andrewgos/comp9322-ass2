package au.gov.nsw.rms.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Entry")
public class PaymentEntry {
    private String _pid;
    private String _nid;
    private String Amount;
    private String CreditCardDetails;
    private String PaidDate;
    
    
	public String get_pid() {
		return _pid;
	}
	@XmlElement(name="_pid")
	public void set_pid(String _pid) {
		this._pid = _pid;
	}
	public String get_nid() {
		return _nid;
	}
	@XmlElement(name="_nid")
	public void set_nid(String _nid) {
		this._nid = _nid;
	}
	public String getAmount() {
		return Amount;
	}
	@XmlElement(name="Amount")
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getCreditCardDetails() {
		return CreditCardDetails;
	}
	@XmlElement(name="CreditCardDetails")
	public void setCreditCardDetails(String creditCardDetails) {
		CreditCardDetails = creditCardDetails;
	}
	public String getPaidDate() {
		return PaidDate;
	}
	@XmlElement(name="PaidDate")
	public void setPaidDate(String paidDate) {
		PaidDate = paidDate;
	}


}
