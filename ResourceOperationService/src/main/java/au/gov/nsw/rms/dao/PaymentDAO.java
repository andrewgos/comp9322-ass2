package au.gov.nsw.rms.dao;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import au.gov.nsw.rms.model.Payment;
import au.gov.nsw.rms.model.PaymentEntry;

public class PaymentDAO {
	private static final String XML_PATH = "src/main/resources/xml/Payments.xml";

    public static Payment getPayment(){
    	// UNMARSHALLING XML INTO OBJECT
		Payment payment= null;
		File xmlFile = new File(XML_PATH);
		try {
			JAXBContext context = JAXBContext.newInstance(Payment.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			payment = (Payment) unmarshaller.unmarshal(xmlFile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return payment;
    }
    
    public static PaymentEntry getPaymentDetail(String pid){
    	Payment payment = getPayment();
    	
    	for(int i =0; i<payment.getEntries().size();i++){
    		if(payment.getEntries().get(i).get_pid().equals(pid)){
    			return payment.getEntries().get(i);
    		}
    	}
    	
		return null;
    }
    
    public static void setPayment(Payment newPayment){
		File xmlFile = new File(XML_PATH);
		try {
			JAXBContext context = JAXBContext.newInstance(Payment.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(newPayment, xmlFile);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
    }
    
    public static void updatePayment(String pid, String creditCardDetail){
    	Payment payment = getPayment();
    	
		for(int i = 0 ; i<payment.getEntries().size();i++){
    		if(payment.getEntries().get(i).get_pid().equals(pid)){
    			//UPDATE CREDIT CARD DETAIL
    			payment.getEntries().get(i).setCreditCardDetails(creditCardDetail);
    			
    			//UPDATE DATE
    			Date currDate = new Date();
    			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    			payment.getEntries().get(i).setPaidDate(dateFormat.format(currDate));
    			break;
    		}
    	}
		setPayment(payment);
    }
    
    

}
