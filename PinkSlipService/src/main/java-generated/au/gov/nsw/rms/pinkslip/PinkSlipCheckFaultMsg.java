
package au.gov.nsw.rms.pinkslip;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.0.4
 * 2015-05-16T19:38:43.149+10:00
 * Generated source version: 3.0.4
 */

@WebFault(name = "pinkSlipCheckFault", targetNamespace = "http://rms.nsw.gov.au/pinkslipdefinition")
public class PinkSlipCheckFaultMsg extends Exception {
    
	private static final long serialVersionUID = -1519121977707586477L;
	private au.gov.nsw.rms.pinkslipdefinition.ServiceFaultType pinkSlipCheckFault;

    public PinkSlipCheckFaultMsg() {
        super();
    }
    
    public PinkSlipCheckFaultMsg(String message) {
        super(message);
    }
    
    public PinkSlipCheckFaultMsg(String message, Throwable cause) {
        super(message, cause);
    }

    public PinkSlipCheckFaultMsg(String message, au.gov.nsw.rms.pinkslipdefinition.ServiceFaultType pinkSlipCheckFault) {
        super(message);
        this.pinkSlipCheckFault = pinkSlipCheckFault;
    }

    public PinkSlipCheckFaultMsg(String message, au.gov.nsw.rms.pinkslipdefinition.ServiceFaultType pinkSlipCheckFault, Throwable cause) {
        super(message, cause);
        this.pinkSlipCheckFault = pinkSlipCheckFault;
    }

    public au.gov.nsw.rms.pinkslipdefinition.ServiceFaultType getFaultInfo() {
        return this.pinkSlipCheckFault;
    }
}
