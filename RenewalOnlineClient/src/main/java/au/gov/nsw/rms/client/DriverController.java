package au.gov.nsw.rms.client;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import au.gov.nsw.rms.bean.Notice;

@Controller
@RequestMapping(value = "/driver")
public class DriverController {
	private static final String REST_URI = "http://localhost:8080/ResourceOperationService";
	
	private String nid = null;
	private String rid = null;
	private static final String DRIVER_KEY = "ddd-123";
	
	@RequestMapping(value = "/{nid}")
	public String viewDashboard(ModelMap model, HttpServletRequest httpRequest, @PathVariable("nid") String nid) throws JsonProcessingException, IOException {
		this.nid = nid;
		String noticeStatus = "";
		
		// GET NOTICE DETAIL
		WebClient webClient = WebClient.create(REST_URI);
		webClient.path("/notice/"+nid).accept(MediaType.APPLICATION_JSON);
		webClient.header("Key", DRIVER_KEY);
		Response response = webClient.get();
		
		if(response.getStatus() == Status.OK.getStatusCode()) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree(response.readEntity(String.class));
			this.rid = rootNode.get("_rid").getTextValue();
			noticeStatus = rootNode.get("status").getTextValue();
			
			model.addAttribute("_nid", rootNode.get("_nid").getTextValue());
			model.addAttribute("_rid", rootNode.get("_rid").getTextValue());
			model.addAttribute("status", rootNode.get("status").getTextValue());
		}
   		
		// GET CAR REGISTRATION DETAIL
   		webClient.back(true);
		webClient.path("/car/"+rid).accept(MediaType.APPLICATION_JSON);
		
		response = webClient.get();
		if(response.getStatus() == Status.OK.getStatusCode()){
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree(response.readEntity(String.class));
			
			// _rid already added to attribute
			model.addAttribute("regoNumber", rootNode.get("regoNumber").getTextValue());
			model.addAttribute("regoValidity", rootNode.get("regoValidity").getTextValue());
			JsonNode driverNode = rootNode.get("driver");
			model.addAttribute("email", driverNode.get("email").getTextValue());
			model.addAttribute("lastName", driverNode.get("lastName").getTextValue());
			model.addAttribute("firstName", driverNode.get("firstName").getTextValue());
			model.addAttribute("driverLicenseNo", driverNode.get("driverLicenseNo").getTextValue());
		}
		
		// GET PAYMENT DETAIL
		if(noticeStatus.equals(Notice.Status.AWAITING_PAYMENT.name())){
	   		webClient.back(true);
			webClient.path("/payment/"+rid+nid).accept(MediaType.APPLICATION_JSON);
			response = webClient.get();
			if(response.getStatus() == Status.OK.getStatusCode()){
				ObjectMapper mapper = new ObjectMapper();
				JsonNode rootNode = mapper.readTree(response.readEntity(String.class));
				
				// _nid already added to attribute
				model.addAttribute("amount", rootNode.get("amount").getTextValue());
				model.addAttribute("_pid", rootNode.get("_pid").getTextValue());
			}

		}
		
		
		return "driver_dashboard";

	}

	@RequestMapping(value = "/update")
	public String updateRenewalNotice(ModelMap model, HttpServletRequest httpRequest) throws JsonProcessingException, IOException {
		WebClient webClient = WebClient.create(REST_URI);
		webClient.header("Key", DRIVER_KEY);
		Response response = null;
		
		String newStatus = "";
		
		if(httpRequest.getParameter("action").equalsIgnoreCase("request")){
			newStatus = Notice.Status.REQUESTED.name();
			webClient.path("/notice/"+this.nid+"/"+newStatus);
			response = webClient.put(null);
		}
		else if(httpRequest.getParameter("action").equalsIgnoreCase("cancel")){
			newStatus = Notice.Status.CANCELLED.name();
			webClient.path("/notice/"+this.nid+"/"+newStatus);
			response = webClient.put(null);
		}
		else if(httpRequest.getParameter("action").equalsIgnoreCase("archive")){
			newStatus = Notice.Status.ARCHIVED.name();
			webClient.path("/notice/"+this.nid);
			response = webClient.delete();
		}
		else{
			return "../index";
		}
		
		
		viewDashboard(model, httpRequest, nid);
		
		
		return "driver_dashboard";
	}


	@RequestMapping(value = "/pay")
	public String makePayment(ModelMap model, HttpServletRequest httpRequest) throws JsonProcessingException, IOException {
		WebClient webClient = WebClient.create(REST_URI);
		webClient.header("Key", DRIVER_KEY);
		
		if(httpRequest.getMethod().equalsIgnoreCase("post")){
			String creditCardDetail = httpRequest.getParameter("creditcarddetail");
			String regoValidity = httpRequest.getParameter("regovalidity");
			
			// INCREMENT YEAR BY 1
			String[] regoDate = regoValidity.split("/");
			int year = Integer.parseInt(regoDate[2]);
			year++;
			regoDate[2] = String.valueOf(year);
			
			
			// UPDATE BY REST SERVICE
			webClient.path("/payment/"+this.rid+"/"+this.nid+"/"+creditCardDetail);
			Response response = webClient.put(null);
			
			webClient.back(true);
			webClient.path("/car/"+rid+"/"+regoDate[0]+"/"+regoDate[1]+"/"+regoDate[2]);
			response = webClient.put(null);

			viewDashboard(model, httpRequest, nid);
		}
		else{
			return "../index";
		}
		
		

		return "driver_dashboard";
	}
	
	
	
}
