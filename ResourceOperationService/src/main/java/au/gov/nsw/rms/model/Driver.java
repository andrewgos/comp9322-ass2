package au.gov.nsw.rms.model;

import javax.xml.bind.annotation.*;


@XmlRootElement(name="Driver")
public class Driver
{
	private String email;
	private String lastName;
	private String firstName;
	private String driverLicenseNo;
	
	public String getLastName()
	{
		return lastName;
	}

	@XmlElement(name="LastName")
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getFirstName()
	{
		return firstName;
	}

	@XmlElement(name="FirstName")
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getDriverLicenseNo()
	{
		return driverLicenseNo;
	}

	@XmlElement(name="DriversLicenseNo")
	public void setDriverLicenseNo(String driverLicenseNo)
	{
		this.driverLicenseNo = driverLicenseNo;
	}
	
	public String getEmail()
	{
		return email;
	}

	@XmlElement(name="Email")
	public void setEmail(String email)
	{
		this.email = email;
	}
	
}
