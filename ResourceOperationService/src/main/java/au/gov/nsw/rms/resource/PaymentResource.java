package au.gov.nsw.rms.resource;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import au.gov.nsw.rms.dao.NoticeDAO;
import au.gov.nsw.rms.dao.PaymentDAO;
import au.gov.nsw.rms.model.Notice;
import au.gov.nsw.rms.model.Payment;
import au.gov.nsw.rms.model.PaymentEntry;


@Path("/payment")
public class PaymentResource {

	private static final String DRIVER_KEY = "ddd-123";
	private static final String OFFICER_KEY = "ooo-123";

	@POST
    @Path("/create")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response newPayment(
					@FormParam("nid") String nid,
					@FormParam("rid") String rid,
					@FormParam("amount") String amount,
					@HeaderParam("Key") String headerKey){
		
		//AUTHENTICATION CHECK
		if(headerKey == null || !headerKey.equals(OFFICER_KEY)){
    		Response response = Response.status(Status.UNAUTHORIZED).build();
        	return response;
    	}
		
		// GET PAYMENT OBJECT
		Payment payment = PaymentDAO.getPayment();
		
		// IF ALREADY EXISTS
		if(payment.getPaymentEntry(rid+nid) != null){
			Response response = Response.status(Status.BAD_REQUEST).build();
			return response;
		}
		
		// GENERATE NEW PAYMENT ENTRY
		PaymentEntry newPaymentEntry = new PaymentEntry();
		newPaymentEntry.set_pid(rid+nid);
		newPaymentEntry.set_nid(nid);
		newPaymentEntry.setAmount(amount);
		newPaymentEntry.setCreditCardDetails("NULL");
		newPaymentEntry.setPaidDate("NULL");

		payment.addEntry(newPaymentEntry);		
		PaymentDAO.setPayment(payment);
		
		// THIS SEND URL FOR DRIVER TO SEE HIS NOTICE AND ABLE TO SEE HIS PAYMENT
		String responseUri = "http://localhost:8080/RenewalOnlineClient/driver/"+nid;
    	Response response = Response.status(Status.CREATED).entity(responseUri).build();

		return response;
	}
	

	 @PUT
	 @Path("{rid}/{nid}/{creditcarddetail}")
	 public Response updatePayment(@PathParam("rid") String rid, @PathParam("nid") String nid, @PathParam("creditcarddetail") String creditCardDetail, @HeaderParam("Key") String headerKey) {
		 //AUTHENTICATION CHECK
		 if(headerKey == null || !headerKey.equals(DRIVER_KEY)){
			 Response response = Response.status(Status.UNAUTHORIZED).build();
			 return response;
		 }
		 
		 String pid = rid+nid;
		 PaymentEntry paymentEntry = PaymentDAO.getPaymentDetail(pid);
		 
		 // IF NOT FOUND
		 if(paymentEntry == null){
			 Response response = Response.status(Status.NOT_FOUND).build();
			 return response;
		 }
		 
		 // IF NOTICE STATUS IS NOT AWAITING PAYMENT
		 if(!NoticeDAO.getNoticeStatus(nid).equals(Notice.Status.AWAITING_PAYMENT.name())){
			 Response response = Response.status(Status.NOT_MODIFIED).build();
			 return response;
		 }
		 
		 // IF FIELD IS NOT NULL (NULL = AVAILABLE TO FILL IN)
		 if(!paymentEntry.getCreditCardDetails().equals("NULL")||
			!paymentEntry.getPaidDate().equals("NULL")){
			 Response response = Response.status(Status.NOT_MODIFIED).build();
			 return response;
		 }
		 
		 PaymentDAO.updatePayment(pid, creditCardDetail);
		 NoticeDAO.updateNotice(nid, Notice.Status.COMPLETED.name());
		 
		 Response response = Response.status(Status.OK).build();
		 return response;

	 }
	
	
	
	 @GET
	 @Path("{pid}")
	 @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	 public Response getPaymentDetail(@PathParam("pid") String pid, @HeaderParam("Key") String headerKey) {
		 // CHECK AUTHENTICATION
		 if(headerKey == null || (!headerKey.equals(DRIVER_KEY) && !headerKey.equals(OFFICER_KEY))){
			 Response response = Response.status(Status.UNAUTHORIZED).build();
			 return response;
		 }
		 
		 PaymentEntry paymentEntry = PaymentDAO.getPaymentDetail(pid);
		 
		 // IF NOT FOUND
		 if(paymentEntry == null){
			 Response response = Response.status(Status.NOT_FOUND).build();
			 return response;
		 }
		 
		 Response response = Response.status(Status.OK).entity(paymentEntry).build();
		 return response;
	 }
	
	

}

