package au.gov.nsw.rms.greenslip.model;


public class Entry
{
	private String _cid;
	private String regoNumber;
	private Driver driver;
	private Insurance insurance;
	public String get_cid() {
		return _cid;
	}
	public void set_cid(String _cid) {
		this._cid = _cid;
	}
	public String getRegoNumber() {
		return regoNumber;
	}
	public void setRegoNumber(String regoNumber) {
		this.regoNumber = regoNumber;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Insurance getInsurance() {
		return insurance;
	}
	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

}
