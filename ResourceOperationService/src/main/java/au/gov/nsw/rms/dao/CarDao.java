package au.gov.nsw.rms.dao;

import java.io.*;
import javax.xml.bind.*;
import au.gov.nsw.rms.model.*;


public class CarDao
{
	private static final String XML_PATH = "src/main/resources/xml/Car_Registrations.xml";
	
	public Registration getInstance()
	{
		Registration registration = null;
		File xmlFile = new File(XML_PATH);
		
		try
		{
			JAXBContext context = JAXBContext.newInstance(Registration.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			registration = (Registration) unmarshaller.unmarshal(xmlFile);
		}
		catch(JAXBException e)
		{
			System.err.println(e.getMessage());
		}
		
		return registration;
	}
	
	public void saveCar(Registration entry)
	{
		File xmlFile = new File(XML_PATH);
		
		try
		{
			JAXBContext context = JAXBContext.newInstance(Registration.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(entry, xmlFile);
		}
		catch (JAXBException e)
		{
			System.err.println(e.getMessage());
		}
	}
	
}
